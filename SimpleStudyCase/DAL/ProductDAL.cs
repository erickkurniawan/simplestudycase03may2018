﻿using Microsoft.Extensions.Configuration;
using SimpleStudyCase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;

namespace SimpleStudyCase.DAL
{
    public class ProductDAL : IProduct
    {
        private SqlConnection conn;
        private IConfiguration _config;
        private string connStr;
        public ProductDAL(IConfiguration config)
        {
            _config = config;
            connStr = _config.GetConnectionString("DefaultConnection");
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetAll()
        {
            using(conn = new SqlConnection(connStr))
            {
                string strSql = @"select * from Products
                                  order by ProductName asc";
                var results = conn.Query<Product>(strSql);
                return results;
            }
        }

        public Product GetById(int id)
        {
            using(conn=new SqlConnection(connStr))
            {
                string strSql = @"select * from Products 
                                  where ProductID=@ProductID";
                var param = new { ProductID = id };
                var result = conn.QuerySingleOrDefault<Product>(strSql,param);
                return result;
            }
        }

        public void Insert(Product product)
        {
            using(conn=new SqlConnection(connStr))
            {
                string strSql = @"insert into Products(ProductName,Description,Price,Quantity) 
                                  values(@ProductName,@Description,@Price,@Quantity)";
                var param = new
                {
                    ProductName = product.ProductName,
                    Description = product.Description,
                    Price = product.Price,
                    Quantity = product.Quantity
                };
                try
                {
                    conn.Execute(strSql, param);
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception("Error: "+sqlEx.Message);
                }
            }
        }

        public void Update(int id, Product product)
        {
            var currUpdate = GetById(id);
            if (currUpdate != null)
            {
                using (conn = new SqlConnection(connStr))
                {
                    string strSql = @"update Products set ProductName=@ProductName,
                    Description=@Description,Price=@Price,Quantity=@Quantity 
                    where ProductID=@ProductID";

                    var param = new
                    {
                        ProductName = product.ProductName,
                        Description = product.Description,
                        Price = product.Price,
                        Quantity = product.Quantity,
                        ProductID = id
                    };
                    try
                    {
                        conn.Execute(strSql, param);
                    }
                    catch (SqlException sqlEx)
                    {

                        throw new Exception(sqlEx.Message);
                    }
                }
            }
            else
            {
                throw new Exception("Product Not Found !");
            }
        }
    }
}
